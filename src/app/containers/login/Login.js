/**
 * Created by Alexey Zverev on 01.12.16.
 */
import { connect }            from 'react-redux';
import { bindActionCreators } from 'redux';
import {Login} from '../../views'
import * as Actions from '../../redux/actions/login/Login'

const mapStateToProps = (state) => {
    return {
        userName: state.login.userName,
        password: state.login.password,
        wait: state.login.wait
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            checkAccess: Actions.checkAccess
        },
        dispatch
    );
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login);