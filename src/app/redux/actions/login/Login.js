/**
 * Created by Alexey Zverev on 02.12.16.
 */
import * as Actions from '../Action'
import axios from 'axios';

export function checkAccess(userName, password) {

    return (dispatch) => {
        dispatch(
          {
              type: Actions.ACTION_CHECK_ACCESS,
              payload: {
                  userName: userName,
                  password: password,
                  wait: true,
                  success: false
              }
          }
        );

        axios.get('/rest/login.json')
            .then(function (response) {
                const data = response.data;
                dispatch(checkAccessSuccessful(data));
            })
            .catch(function (response) {
                console.log(response);
                dispatch(checkAccessFailed());
            });

    };
}

function checkAccessFailed() {
    return {
        type: Actions.ACTION_CHECK_ACCESS_FAILED,
        payload: {
            wait: false,
            success: false
        }
    }
}


function checkAccessSuccessful(data) {
    return {
        type: Actions.ACTION_CHECK_ACCESS_SUCCESSFUL,
        payload: {
            wait: false,
            success: true,
            token: data.token
        }
    }
}