/**
 * Created by Alexey Zverev on 02.12.16.
 */

export const ACTION_CHECK_ACCESS = "ACTION_CHECK_ACCESS";
export const ACTION_CHECK_ACCESS_SUCCESSFUL = "ACTION_CHECK_ACCESS_SUCCESSFUL";
export const ACTION_CHECK_ACCESS_FAILED = "ACTION_CHECK_ACCESS_FAILED";
