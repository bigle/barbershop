/**
 * Created by Alexey Zverev on 01.12.16.
 */
import {createStore, applyMiddleware, compose} from "redux";
import createLogger from "redux-logger";
import thunkMiddleware from "redux-thunk";
import rootReducer from '../reducers';

const loggerMiddleware = createLogger({
    level: 'info',
    collapsed: true
});

const enhancer = compose(
    applyMiddleware(thunkMiddleware, loggerMiddleware) // logger after thunk to avoid undefined actions
);

export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, enhancer);
    return store;
}