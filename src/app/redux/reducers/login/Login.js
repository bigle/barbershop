/**
 * Created by Alexey Zverev on 01.12.16.
 */
import * as Actions from '../../actions/Action'

const initialState = {
    userName: "",
    password: "",
    wait: false,
    success: false
}

export default function login(state = initialState, action) {
    switch (action.type) {
        case Actions.ACTION_CHECK_ACCESS:
            var newState = { ...state,
                userName: action.payload.userName,
                password:  action.payload.password,
                wait: action.payload.wait,
                success: action.payload.success,
                token: action.payload.token
            };
            return newState;
        case Actions.ACTION_CHECK_ACCESS_FAILED:
            return {
                ...state,
                userName: action.payload.userName,
                password:  action.payload.password,
                wait: action.payload.wait,
                success: action.payload.success
            };
        case Actions.ACTION_CHECK_ACCESS_SUCCESSFUL:
            return {
                ...state,
                userName: action.payload.userName,
                password:  action.payload.password,
                wait: action.payload.wait,
                success: action.payload.success,
                token: action.payload.token
            };
        default:
            return state;
    }
}