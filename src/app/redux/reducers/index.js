/**
 * Created by Alexey Zverev on 01.12.16.
 */
import { combineReducers } from 'redux'
import login from './login/Login';

export default combineReducers({
    login
})