import React from "react";
import {Route, IndexRoute} from "react-router";
import {App, Login} from "../containers";
import {Home, About, Components, Thumbnail} from "../views";

const Routes = () => {
    return (
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>
            <Route path="/components" component={Components}/>
            <Route path="/about" component={About}/>
            <Route path="/login" component={Login}/>
            <Route path="/thumbnail" component={Thumbnail}/>
        </Route>
    );
};

export default Routes;
