/**
 * Created by Alexey Zverev on 03.12.16.
 */

import React, {Component, PropTypes} from "react";
import ReactDOM from 'react-dom';
import  {Div, ThumbnailsScroll} from '../../components';
import ThumbnailItem from "../../components/ThumbnailsScroll/ThumbnailItem";
import ThumbnailPanel from "../../components/ThumbnailsScroll/ThumbnailPanel";
import TN from '../../components/ThumbnailsScroll/Thumbnail'

var _index = 0;

class Thumbnail extends Component {

    constructor(props) {
        super(props);

        this.state = {
            items: [],
            hasMore: true,
            columns: []
        }
    }

    handlerOnChange() {
        const col1 = $("#col1");
    }

    handleResizeCol(width) {
        console.log(width);
    }

    componentDidMount() {
        //window.addEventListener("resize", ::this.handlerOnChange);
        let {columns} = this.state;
        let el = ReactDOM.findDOMNode(this.item);
        console.log(el.offsetWidth);
        for(let column of columns) {
            let item = column;
            let el = ReactDOM.findDOMNode(column);
            console.log(el);
        }
    }

    _renderMessages() {
        console.log("Thumbnail ");
        let result = <div></div>
        if(this.state.items && this.state.items.length > 0) {
            result = <ThumbnailPanel items = { this.state.items }/>;
        }
        return result;
    }

    _putItem(items, item) {
        let currentIndex = 0;
        let height = 0;
        let first = true;
        for(let zitems of items) {
            if (first) {
                height = zitems.height;
                first = false;
            } else {
                height = Math.min(height, zitems.height);
            }

        }
        for(let zitems of items) {
            if(height == zitems.height) {
                break;
            };
            currentIndex++;
        }
        console.log(currentIndex);
        let zitem = items[currentIndex];
        zitem.items = zitem.items.concat(item);
        zitem.height += item.height;
        items[currentIndex] = zitem;
        return items;
    }

    _loadMore() {
        console.log('load');
        let currentItems = this.state.items;
        if (currentItems.length == 0) {
            currentItems = [{height: 0,items: []},{height: 0,items: []},{height: 0,items: []}];
        }

        _index++;

        let wh = Math.floor(Math.random() * (300 - 100 + 1) + 100);
        let newItems = {name: "item_"+_index, id: _index, width: wh, height: wh};
        console.log(newItems);

        currentItems = this._putItem(currentItems, newItems);
        console.log(currentItems);


        this.setState({
            hasMore: _index < 20,
            items: currentItems
        });

        // setTimeout(function () {
        //     this.setState({
        //         items: currentItems.concat(newItems),
        //         hasMore: false
        //     });
        // }.bind(this), 1000);

    }

    _render() {
        let column = <Div className="col-xs-12 col-sm-6 col-md-4 col-lg-4">
            <a href="#" className= "thumbnail">
                <img src="/rest/test.jpg" data-src="holder.js/100%x180" style={{width: "100%", display: "block"}}/>
            </a>
        </Div>;
        this.state.columns = this.state.columns.concat(column);
        return this.state.columns;
    }

    render() {
        const style = `.test{
            height: 180px; width: 100%; display: block;
        }`;

        return <div className="container-fluid container-fixed">

            <Div className="row">

                <TN ref={(item) => {this.item = item; }}/>
                {/*<ThumbnailsScroll threshold={0} loadMore={::this._loadMore} hasMore={this.state.hasMore} elementIsScrollable={false}>*/}
                        {/*{this._renderMessages()}*/}
                {/*</ThumbnailsScroll>*/}
            </Div>
        </div>
    }

}

export default Thumbnail;