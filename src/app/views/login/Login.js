import React, {Component, PropTypes} from "react";
import {Input, Const, Button} from "../../components";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = { userName: props.userName, password: props.password};
    }

    componentDidMount() {
        this.state = { userName: this.props.userName, password: this.props.password};
    }

    onSubmitForm(e) {
        e.preventDefault();
        this.props.checkAccess( this.state.userName, this.state.password);
    }

    handleChangeUserName(e) {
        this.setState({userName: e.target.value});
        e.preventDefault();
    }

    handleChangePassword(e) {
        this.setState({password: e.target.value});
        e.preventDefault();
    }

    render() {
        const {userName, password, wait} = this.props;
        console.log("Render Login");
        return <div className="container">
            <form className="form-signin" onSubmit={ ::this.onSubmitForm }>
                <fieldset>
                    <h2>Please sign in</h2>
                    <Input disabled={ wait } placeholder="UserName" type={ Const.INPUT_TYPE.USER_NAME }
                           value={ this.state.userName } onChange={ ::this.handleChangeUserName }/>
                    <br/>
                    <Input disabled={ wait } placeholder="Password" type={ Const.INPUT_TYPE.PASSWORD }
                           value={ this.state.password } onChange={ ::this.handleChangePassword }/>
                    <br/>
                    <Button disabled={ wait } spinner={ wait }>Sign</Button>
                </fieldset>
            </form>
        </div>
    }

}

Login.propTypes = {
    userName: PropTypes.string,
    password: PropTypes.string,
    wait: PropTypes.bool,
    checkAccess: PropTypes.func.isRequired
}

export default Login;
