import About from "./about/About";
import Home from "./home/Home";
import Login from "./login/Login";
import Components from "./components/Components";
import Thumbnail from "./thumbnails/Thumbnails";

export {
    About,
    Home,
    Login,
    Components,
    Thumbnail
};
