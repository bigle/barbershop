import React, {Component} from "react";

var _id = 0;

class BaseComponent extends Component {

    static getUniqueId() {
        return _id++;
    }

}

export default BaseComponent;
