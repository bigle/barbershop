import React, {Component, PropTypes} from "react";
import BaseComponent from "../base/BaseComponent";
import {Const} from "../index";

class Input extends BaseComponent {

    render() {
        let {placeholder, type, disabled, value, onChange} = this.props;
        let addonIcon = null;
        let addonGroup = null;

        if (Const.INPUT_TYPE.USER_NAME == type) {
            addonIcon = <i className="fa fa-user"/>
        }
        if (Const.INPUT_TYPE.PASSWORD == type) {
            addonIcon = <i className="fa fa-lock"/>
        }

        if ((!type) || (Const.INPUT_TYPE.USER_NAME == type)) {
            type = Const.INPUT_TYPE.TEXT;
        }

        if (addonIcon) {
            addonGroup = <span className="input-group-addon">{ addonIcon }</span>;
        }

        return <div className="input-group">
            { addonGroup }
            <input type={ type } className="form-control" placeholder={ placeholder } disabled= { disabled } value={ value } onChange={onChange}/>
        </div>
    }

}

Input.propTypes = {
    placeholder: PropTypes.string,
    type: PropTypes.string,
    disabled: PropTypes.bool,
    value: PropTypes.string,
    onChange: PropTypes.func
}

export default Input;