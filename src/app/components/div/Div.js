/**
 * Created by Alexey Zverev on 10.12.16.
 */
import React, {Component, PropTypes} from "react";
import BaseComponent from "../base/BaseComponent";
import ReactDOM from 'react-dom';

class Div extends BaseComponent {

    constructor(props) {
        super(props);
        const id = BaseComponent.getUniqueId();
        this.state = {id: id};
        this.getWidth = ::this.getWidth;
    }

    handlerOnChange() {
        const col = $("#"+this.state.id);
        const { onResize } = this.props;
        if (onResize) {
            onResize(col.width());
        };
    }

    getWidth() {
        console.log("1111");
    }

    componentDidMount() {
        window.addEventListener("resize", ::this.handlerOnChange);
        let el = ReactDOM.findDOMNode(this);
        //this.props.width = el.offsetWidth;
        console.log("Div componentDidMount");
        console.log(el.offsetWidth);


        const objects = React.Children.toArray(this.props.children);
        console.log(objects[0].props);
    }

    render() {
        const {className} = this.props;
        return <div id={this.state.id} className={className}>
            {this.props.children}
        </div>
    }

}

Div.PropTypes = {
    children: PropTypes.node,
    onResize: PropTypes.fun,
    width: PropTypes.number,
    getWidth: PropTypes.fun
};

export default Div;