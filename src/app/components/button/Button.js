/**
 * Created by Alexey Zverev on 02.12.16.
 */
import React, {Component, PropTypes} from "react";
import BaseComponent from "../base/BaseComponent";

class Button extends BaseComponent {

    render() {
        const { type, children, disabled, spinner } = this.props;
        const indicator = spinner ? <i className="fa fa-spinner fa-spin"></i> : null;
        return <button className="btn btn-primary btn-block" type={ type } disabled = { disabled }>
            { indicator }
            { children }
            </button>
    }

}

Button.PropTypes = {
    type: PropTypes.string,
    disabled: PropTypes.bool,
    spinner: PropTypes.bool
}

export default Button;