const Const = {
    INPUT_TYPE: {
        TEXT: "text",
        PASSWORD: "password",
        USER_NAME: "userName"
    }
};

export default Const;