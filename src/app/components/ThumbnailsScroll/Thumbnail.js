/**
 * Created by Alexey Zverev on 19.12.16.
 */
import React, {Component, PropTypes} from "react";
import ReactDOM from 'react-dom';

class Thumbnail extends Component {

    componentDidMount() {
        let el = ReactDOM.findDOMNode(this);
        console.log("componentDidMount");
        console.log(el.offsetWidth);
    }

    render() {
        console.log("render");
        const img = "/rest/test.jpg";
        return <div>
            <a href="#" className="thumbnail" style={{backgroundImage: `url("${img}")`, height: '300px', width: '100%',backgroundColor: 'rgb(34, 37, 32)'}}>

            </a>
        </div>
    }

}

export default Thumbnail;