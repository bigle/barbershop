/**
 * Created by Alexey Zverev on 18.12.16.
 */
import React, {Component, PropTypes} from "react";
import ThumbnailList from "./ThumbnailList";

class ThumbNailPanel extends Component {

    constructor(props) {
        super(props);
        //this.state = {columns: props.columns, items: []};
    }

    render() {
        const { items } = this.props;
        console.log("ThumbNailPanel");
        let innerItems = items.map((item, i) => {
            let tlClassName = (i == 0) ? "col-xs-12 col-sm-6 col-md-4 col-lg-4" : "col-xs-12 col-sm-6 col-md-4 col-lg-4";
            return <ThumbnailList className={ tlClassName } items={ item.items }/>
        });
        return <div>
            { innerItems }
        </div>
    }

}

ThumbNailPanel.PropTypes = {
    columns: PropTypes.number.isRequired,
    items: React.PropTypes.array
}

ThumbNailPanel.defaultProps = {
    items: []
}

export default ThumbNailPanel;