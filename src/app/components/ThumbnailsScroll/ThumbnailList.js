/**
 * Created by Alexey Zverev on 14.12.16.
 */
import React, {Component, PropTypes} from "react";
import ReactDOM from 'react-dom';
import ThumbnailItem from "./ThumbnailItem";

class ThumbnailList extends Component {

    _findElement() {
        return ReactDOM.findDOMNode(this);
    }

    componentDidMount() {

    }

    render() {
        console.log("ThumbnailList");

        const { items, className } = this.props;
        let result = <div></div>;
        if(items && items.length > 0) {
            result = items.map((item) => {
                return <ThumbnailItem item={ item }/>;
            });
        }
        return <div className={ className }>{ result }</div>;
    }

}

ThumbnailList.PropTypes = {
    items: React.PropTypes.array,

}

export default ThumbnailList;