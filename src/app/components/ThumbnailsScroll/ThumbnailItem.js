/**
 * Created by Alexey Zverev on 14.12.16.
 */
import React, {Component, PropTypes} from "react";
import ReactDOM from 'react-dom';
class ThumbnailItem extends React.Component {

    render() {
        const el = ReactDOM.findDOMNode(this);
        const { className, children, item } = this.props;
        console.log("ThumbnailItem");

        return <a href="#" className= { className }>
            <img src="/rest/171x180.svg" data-src="holder.js/100%x180" style={{width: "100%", display: "block"}}/>
            { item.name }
        </a>;
    }

}

ThumbnailItem.PropTypes = {
    className: React.PropTypes.string,
    height: React.PropTypes.number,
    width: React.PropTypes.number,
    item: React.PropTypes.object
}

ThumbnailItem.defaultProps = {
    className: "thumbnail",
    children: ""
}

export default ThumbnailItem;