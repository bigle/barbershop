import React from "react";
import {render} from "react-dom";
import injectTpEventPlugin from "react-tap-event-plugin";
import {AppContainer} from "react-hot-loader";
import Root from "./Root";
import routes from "./routes/Routes";
import "babel-polyfill";
import "animate.css";
import "jquery";
import "font-awesome/css/font-awesome.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./style/index.scss";
import {Provider} from "react-redux";
import configureStore from './redux/store/configureStore'
import "lightbox2/dist/css/lightbox.min.css"
import "lightbox2/dist/js/lightbox.min"

const ELEMENT_TO_BOOTSTRAP = 'root';
const BootstrapedElement = document.getElementById(ELEMENT_TO_BOOTSTRAP);
const store = configureStore();


injectTpEventPlugin();

const renderApp = appRoutes => {
    render(<Provider store={store}>
            <AppContainer>
                <Root routes={appRoutes}/>
            </AppContainer>
        </Provider>,
        BootstrapedElement
    );
};

renderApp(routes);

if (module.hot) {
    module.hot.accept(
        './routes/Routes',
        () => {
            const newRoutes = require('./routes/Routes');
            renderApp(newRoutes);
        }
    );
}
